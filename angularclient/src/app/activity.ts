export class Activity {
    id!: number;
    reportId!: number;
    name!: string;
    date!: string;
    place!: string;
    status!: string;
    qualRes!: string;
    quanRes!: string;
}
