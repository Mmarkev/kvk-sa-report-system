import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { SessionStorageService } from '../service/session-storage.service';
import { UserService } from '../service/user-service';
import { User } from '../user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-user-info',
  templateUrl: './edit-user-info.component.html',
  styleUrls: ['./edit-user-info.component.css']
})
export class EditUserInfoComponent implements OnInit {

  loggedUser!: User;
  oldpassword!: string;
  newpassword!: string;
  newpasswordrepeat!: string;
  userId!: number;
  user!: User;

  constructor(public loginService: AuthenticationService, private sessionUser: SessionStorageService, private router: Router,
    private userService: UserService, private route: ActivatedRoute,
    private location: Location) {
    this.userId = this.route.snapshot.params['id'];
    this.userService.getUser(this.userId).subscribe(data => {
      this.user = data;
    });
  }

  ngOnInit(): void {
    this.loggedUser = this.sessionUser.getUser();
  }

  onSubmitUserInfo() {
    this.updateUser();
  }

  onSubmitPassword() {
    this.updateUserPassword();
  }

  updateUser() {
    this.userService.updateUser(this.user.id, this.user)
      .subscribe(data => {
        console.log(data);
        this.user = new User();
        if (this.user.email != this.loggedUser.email) {
          this.router.navigate(['logout']);
        }
        else
        this.goToUserPage();
      }, error => console.log(error));
  }

  updateUserPassword() {
    if (this.newpassword == this.newpasswordrepeat && this.oldpassword == this.loggedUser.password && this.newpassword.length >= 7) {
      this.loggedUser.password = this.newpassword;
      this.userService.updateUserPassword(this.loggedUser.id, this.loggedUser.duties, this.loggedUser)
        .subscribe(data => {
          console.log(data);
          this.loggedUser = new User();
          this.goToUserPage();
        }, error => console.log(error));
    }
  }

  goToUserPage() {
    if (this.loggedUser.duties == "Prezidentas" && this.user.id == this.loggedUser.id
      || this.loggedUser.duties == "Viceprezidentas" && this.user.id == this.loggedUser.id
      || this.loggedUser.duties == "Administratorius" && this.user.id == this.loggedUser.id) {
      this.router.navigate(['userdetails']);
    }

    else if (this.loggedUser.duties == "Prezidentas" && this.user.id != this.loggedUser.id
      || this.loggedUser.duties == "Viceprezidentas" && this.user.id != this.loggedUser.id
      || this.loggedUser.duties == "Administratorius" && this.user.id != this.loggedUser.id) {
      this.router.navigate(['administrate']);
    }

    else if (this.loggedUser.duties != "Prezidentas" && this.loggedUser.duties != "Viceprezidentas" && this.loggedUser.duties != "Administratorius") {
      this.router.navigate(['userdetails']);
    }
  }

  checkIfLogged() {
    return this.loggedUser.id == this.user.id;
  }

  hasAccess() {
    return this.userId == this.loggedUser.id || this.loggedUser.duties == "Prezidentas" || this.loggedUser.duties == "Viceprezidentas" || this.loggedUser.duties == "Administratorius";
  }

}
