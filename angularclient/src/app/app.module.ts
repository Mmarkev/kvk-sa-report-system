import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AddReportComponent } from './add-report/add-report.component';
import { ReportsListComponent } from './reports-list/reports-list.component';
import { UpdateReportComponent } from './update-report/update-report.component';
import { ReportDetailsComponent } from './report-details/report-details.component';
import { AddActivityComponent } from './add-activity/add-activity.component';
import { ActivitiesListComponent } from './activities-list/activities-list.component';
import { UpdateActivityComponent } from './update-activity/update-activity.component';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { LogoutComponent } from './logout/logout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { UserService } from './service/user-service';
import { ReportService } from './service/report-service.service';
import { ActivityService } from './service/activity-service.service';
import { UsersComponent } from './users/users.component';
import { SessionStorageService } from './service/session-storage.service';
import { UserPageComponent } from './user-page/user-page.component';
import { EditUserInfoComponent } from './edit-user-info/edit-user-info.component';
import { AdministrationComponent } from './administration/administration.component';
import { PdfFileComponent } from './pdf-file/pdf-file.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    AddReportComponent,
    ReportsListComponent,
    UpdateReportComponent,
    ReportDetailsComponent,
    AddActivityComponent,
    ActivitiesListComponent,
    UpdateActivityComponent,
    LoginComponent,
    MainpageComponent,
    LogoutComponent,
    NavbarComponent,
    SidenavComponent,
    UsersComponent,
    UserPageComponent,
    EditUserInfoComponent,
    AdministrationComponent,
    PdfFileComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot([])
  ],
  providers: [
    UserService,
    ReportService,
    ActivityService,
    SessionStorageService,
    
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
