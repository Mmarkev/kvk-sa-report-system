import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../activity';
import { ActivityService } from '../service/activity-service.service';

@Component({
  selector: 'app-update-activity',
  templateUrl: './update-activity.component.html',
  styleUrls: ['./update-activity.component.css']
})
export class UpdateActivityComponent implements OnInit {

  id!: number;
  activity: Activity;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private activityService: ActivityService) {
    this.activity = new Activity();
  }

  ngOnInit(): void {
    this.activity = new Activity();

    this.id = this.route.snapshot.params['id'];

    this.activityService.getActivity(this.id)
      .subscribe(data => {
        console.log(data)
        this.activity = data;
      }, error => console.log(error));
  }

  updateActivity() {
    this.activityService.updateReport(this.id, this.activity)
      .subscribe(data => {
        console.log(data);
        this.activity = new Activity();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateActivity();
    this.gotoReport();
  }

  gotoReport() {
    this.router.navigate(['/details', this.activity.reportId]);
  }

}
