import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Report } from '../report';
import { ReportService } from '../service/report-service.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-add-report',
  templateUrl: './add-report.component.html',
  styleUrls: ['./add-report.component.css']
})
export class AddReportComponent implements OnInit {

  report: Report;
  loggedUser!: User;
  committee!: string;
  iduser!: number;

  constructor(
    private router: Router,
    private reportService: ReportService,
    private sessionUser: SessionStorageService) {
    this.report = new Report();
  }

  ngOnInit() {
    this.loggedUser = this.sessionUser.getUser();
    this.committee = this.loggedUser.duties;
    
    this.report.userId = this.loggedUser.id;
    this.report.committee = this.committee;
    this.report.creator = this.loggedUser.name;
  }

  onSubmit() {
    this.reportService.save(this.report).subscribe(result => this.gotoReportList());
  }

  gotoReportList() {
    this.router.navigate(['/reports/Bendros']);
  }

}

