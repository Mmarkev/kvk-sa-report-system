import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Activity } from '../activity';
import { Report } from '../report';
import { ActivityService } from '../service/activity-service.service';
import { ReportService } from '../service/report-service.service';
// import { jsPDF } from 'jspdf';

@Component({
  selector: 'app-pdf-file',
  templateUrl: './pdf-file.component.html',
  styleUrls: ['./pdf-file.component.css']
})
export class PdfFileComponent implements OnInit {

  activities!: Activity[];
  activitiesTemp!: Activity[];
  report!: Report;
  reportId!: number;
  titleDuties!: string;

  constructor(private reportService: ReportService,
    private activityService: ActivityService,
    private route: ActivatedRoute) {
    this.reportId = this.route.snapshot.params['id'];
    this.activities = new Array<Activity>();
    this.titleDuties = "";
  }

  ngOnInit(): void {
    this.getReport();
    this.getActivities();
  }

  getReport() {
    this.reportService.getReport(this.reportId).subscribe(data => {
      this.report = data;
      this.changeformulation();
    }, error => console.log(error));
  }

  getActivities() {
    this.activityService.findActivities().subscribe(data => {
      this.activitiesTemp = data;
      for (let a of this.activitiesTemp) {
        if (this.reportId == a.reportId) {
          this.activities.push(a);
        }
      }
    });
  }

  changeformulation() {
    if (this.report.committee == "Prezidentas") {
      this.titleDuties = "Prezidento"
    }
    else if (this.report.committee == "Viceprezidentas") {
      this.titleDuties = "Viceprezidento"
    }
    else if (this.report.committee == "ARK") {
      this.titleDuties = "Akademinių reikalų komiteto"
    }
    else if (this.report.committee == "SRK") {
      this.titleDuties = "Socialinių reikalų komiteto"
    }
    else if (this.report.committee == "RsV") {
      this.titleDuties = "Ryšių su visuomene komiteto"
    }
    else if (this.report.committee == "ŽIK") {
      this.titleDuties = "Žmogiškųjų išteklių komiteto"
    }
    else if (this.report.committee == "Laisvalaikis") {
      this.titleDuties = "Laisvalaikio"
    }
    else if (this.report.committee == "Marketingas") {
      this.titleDuties = "Marketingo"
    }
    else if (this.report.committee == "Projektai") {
      this.titleDuties = "Projektų"
    }
    else if (this.report.committee == "Finansai") {
      this.titleDuties = "Finansų"
    }
    else if (this.report.committee == "TF SA") {
      this.titleDuties = "Technologijų fakulteto Studentų atstovybės"
    }
    else if (this.report.committee == "VF SA") {
      this.titleDuties = "Verslo fakulteto Studentų atstovybės"
    }
    else if (this.report.committee == "SvMF SA") {
      this.titleDuties = "Sveikatos mokslų fakulteto Studentų atstovybės"
    }

  }


  // For export

  // SITAS NEVEIKE
  /*exportToDocx(element: any, filename = '') {
    var preHtml =  "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
    var postHtml = "</body></html>";
    var html = preHtml + document.getElementById(element)?.innerHTML + postHtml;

    var blob = new Blob(['ufeff', html], { type: 'application/msword' });

    var url = 'data:application/vnd.ms-word;charset=utf-8' + encodeURIComponent(html);

    filename = filename ? filename + '.docx' : 'document.docx';

    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      navigator.msSaveOrOpenBlob(blob, filename);
    }

    else {
      downloadLink.href = url;
      downloadLink.download = filename;
      downloadLink.click();
    }

    document.body.removeChild(downloadLink);
  }*/

  //VEIKIA, BET NE SU LT KALBA
  /*
  @ViewChild('content') el!: ElementRef;
  downloadPDF() {
    let doc = new jsPDF('landscape', 'pt', 'a4');
    doc.html(this.el.nativeElement, {
      callback: (doc) => {
        doc.save("heyheyhey.pdf");
      }
    });
  }*/
}