import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Report } from '../report';
import { ReportService } from '../service/report-service.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-reports-list',
  templateUrl: './reports-list.component.html',
  styleUrls: ['./reports-list.component.css']
})
export class ReportsListComponent implements OnInit {
  //visos ataskaitos:
  reports!: Report[];
  //prisijunges naudotojas:
  loggedUser!: User;
  //filtravimui skirti kintamieji:
  startDate!: string;
  endDate!: string;
  creator!: string;
  areFiltersOn = false;
  //ataskaitų grupės pavadinimas:
  reportGroup!: string;
  //puslapiavimas


  constructor(private reportService: ReportService,
    private router: Router, private sessionUser: SessionStorageService, private route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.reportGroup = this.route.snapshot.params['duties'];
      this.ngOnInit();
    });
  }

  ngOnInit() {
    this.reloadData();
    this.loggedUser = this.sessionUser.getUser();
  }

  reloadData() {
    var reportsGrouped = new Array<Report>();
    //gauna visus reports iš DB
    this.reportService.findAll().subscribe(data => {
      this.reports = data;
      if (this.reportGroup == "Bendros") {
        reportsGrouped = this.reports;
      }
      else {
        for (let report of this.reports) {
          if (report.committee == this.reportGroup) {
            reportsGrouped.push(report);
          }
        }
      }
      this.sortNewToOld(reportsGrouped);
      this.reports = reportsGrouped;
    }
    );
  }

  deleteReport(id: number) {
    this.reportService.deleteReport(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
    this.reloadData();
  }

  //Navigacija
  updateReport(id: number) {
    this.router.navigate(['update', id]);
  }

  reportDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  canUserEdit(report: Report) {
    return (this.loggedUser.duties == report.committee);
  }

  //Rūšiavimas
  sortNewToOld(reports: Array<Report>) {
    reports.sort((a, b) => (a.id > b.id ? -1 : 1));
  }

  sortOldToNew(reports: Array<Report>) {
    reports.sort((a, b) => (a.id < b.id ? -1 : 1));
  }


  //Filtravimas
  onSubmitFilter() {
    let filteredReports = new Array<Report>();
    for (let report of this.reports) {
      //Tik pagal vardą
      if ((report.creator).includes(this.creator) && this.startDate == undefined && this.endDate == undefined) {
        filteredReports.push(report);
        this.areFiltersOn = true;
      }

      //Pagal vardą ir pradžios datą
      else if ((report.creator).includes(this.creator) && this.startDate != undefined && this.endDate == undefined) {
        var compareDateStart = (Number)(this.startDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateStart <= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }

      //Pagal vardą ir pabaigos datą
      else if ((report.creator).includes(this.creator) && this.startDate == undefined && this.endDate != undefined) {
        var compareDateStart = (Number)(this.endDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateStart <= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }

      //Pagal vardą, pradžios ir pabaigos datą
      else if ((report.creator).includes(this.creator) && this.startDate != undefined && this.endDate != undefined) {
        var compareDateStart = (Number)(this.startDate.replace("-", ""));
        var compareDateEnd = (Number)(this.endDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateStart <= reportDate && compareDateEnd >= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }

      //Pagal pradžios datą
      else if (this.creator == undefined && this.startDate != undefined && this.endDate == undefined) {
        var compareDateStart = (Number)(this.startDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateStart <= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }

      //Pagal pradžios ir pabaigos
      else if (this.creator == undefined && this.startDate != undefined && this.endDate != undefined) {
        var compareDateStart = (Number)(this.startDate.replace("-", ""));
        var compareDateEnd = (Number)(this.endDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateStart <= reportDate && compareDateEnd >= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }
      //Pagal pabaigos datą
      else if (this.creator == undefined && this.startDate == undefined && this.endDate != undefined) {
        var compareDateEnd = (Number)(this.endDate.replace("-", ""));
        var reportDate = (Number)((report.date).replace("-", ""));
        if (compareDateEnd >= reportDate) {
          filteredReports.push(report);
        }
        this.areFiltersOn = true;
      }

      else if (this.creator == undefined && this.startDate == undefined && this.endDate == undefined) {
        filteredReports = this.reports;
      }
      else {
        this.areFiltersOn = true;
      }
    }
    this.reports = filteredReports;
  }

  cleanFilters() {
    this.areFiltersOn = false;
    this.ngOnInit();
  }

}