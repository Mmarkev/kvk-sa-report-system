import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  loggedUser! : User;

  constructor(public loginService: AuthenticationService,  private sessionUser: SessionStorageService, private router: Router) {
  }

  ngOnInit() {
    this.loggedUser = this.sessionUser.getUser();
  }

  gotoUpdateUser() {
      this.router.navigate(['editUserInfo', this.loggedUser.id]);
  }
}
