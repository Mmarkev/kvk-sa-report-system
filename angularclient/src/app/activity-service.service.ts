import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Activity } from './activity';

@Injectable({
  providedIn: 'root'
})

export class ActivityService {

  private reportsUrl: string;

  constructor(private http: HttpClient) {
    this.reportsUrl = 'http://localhost:8080/activities';
  }

  public getActivity(id: number): Observable<any> {
    return this.http.get(`${this.reportsUrl}/${id}`);
  }

  public findActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.reportsUrl}`);
  }

  public save(activity: Activity) : Observable<Activity>{
    return this.http.post<Activity>(this.reportsUrl, activity);
  }

  public deleteActivity(id: number): Observable<any> {
    return this.http.delete(`${this.reportsUrl}/${id}`);
  }

  public updateReport(id: number, value: any): Observable<Activity> {
    return this.http.put<Activity>(`${this.reportsUrl}/${id}`, value);
  }

}
