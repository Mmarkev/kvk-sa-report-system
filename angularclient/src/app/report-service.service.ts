import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Report } from './report';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private reportsUrl: string;

  constructor(private http: HttpClient) {
    this.reportsUrl = 'http://localhost:8080/reports';
  }

  public getReport(id: number): Observable<any> {
    return this.http.get(`${this.reportsUrl}/${id}`);
  }

  public updateReport(id: number, value: any): Observable<Report> {
    return this.http.put<Report>(`${this.reportsUrl}/${id}`, value);
  }

  public findAll(): Observable<Report[]> {
    return this.http.get<Report[]>(this.reportsUrl);
  }

  public save(report: Report) {
    return this.http.post<Report>(this.reportsUrl, report);
  }

  public deleteReport(id: number): Observable<any> {
    return this.http.delete(`${this.reportsUrl}/${id}`);
  }
}
