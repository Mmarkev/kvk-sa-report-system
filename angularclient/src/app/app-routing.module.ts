import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivitiesListComponent } from './activities-list/activities-list.component';
import { AddActivityComponent } from './add-activity/add-activity.component';
import { AddReportComponent } from './add-report/add-report.component';
import { AdministrationComponent } from './administration/administration.component';
import { EditUserInfoComponent } from './edit-user-info/edit-user-info.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { PdfFileComponent } from './pdf-file/pdf-file.component';
import { ReportDetailsComponent } from './report-details/report-details.component';
import { ReportsListComponent } from './reports-list/reports-list.component';
import { AuthGaurdService } from './service/auth-gaurd.service';
import { UpdateActivityComponent } from './update-activity/update-activity.component';
import { UpdateReportComponent } from './update-report/update-report.component';
import { UserPageComponent } from './user-page/user-page.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path: 'home', component: MainpageComponent, canActivate:[AuthGaurdService]},
  {path: 'reports/:duties', component: ReportsListComponent},
  {path: 'addreport', component: AddReportComponent, canActivate:[AuthGaurdService]},
  {path: 'update/:id', component: UpdateReportComponent, canActivate:[AuthGaurdService]},
  {path: 'details/:id', component: ReportDetailsComponent, canActivate:[AuthGaurdService]},
  {path: 'details/:id/addactivity', component: AddActivityComponent, canActivate:[AuthGaurdService]},
  {path: 'activities', component: ActivitiesListComponent, canActivate:[AuthGaurdService]},
  {path: 'updateActivity/:id', component: UpdateActivityComponent, canActivate:[AuthGaurdService]},
  {path: '', component: LoginComponent},
  {path: 'userdetails', component: UserPageComponent, canActivate:[AuthGaurdService]},
  {path: 'editUserInfo/:id', component: EditUserInfoComponent, canActivate:[AuthGaurdService]},
  {path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService]},
  {path: 'administrate', component: AdministrationComponent, canActivate:[AuthGaurdService]},
  {path: 'details/:id/toPdf', component: PdfFileComponent, canActivate:[AuthGaurdService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
