import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  loggedUser! : User;

  constructor(private router: Router, public loginService: AuthenticationService,  private sessionUser: SessionStorageService) { }

  ngOnInit(): void {
    this.loggedUser = this.sessionUser.getUser();
  }

  reports(duties: string) {
    this.router.navigate(['reports', duties]);
  }

  canAdd() {
    if (this.loggedUser.duties == "Revizija" || this.loggedUser.duties == "Valdyba") {
      return false;
    }
    else
    return true;
  }
}
