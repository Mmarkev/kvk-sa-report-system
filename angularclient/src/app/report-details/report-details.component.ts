import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../activity';
import { ActivityService } from '../service/activity-service.service';
import { Report } from '../report';
import { ReportService } from '../service/report-service.service';
import { User } from '../user';
import { SessionStorageService } from '../service/session-storage.service';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.css']
})
export class ReportDetailsComponent implements OnInit {

  reportId!: number;
  report!: Report;
  activities!: Activity[];
  reportActivities!: Activity[];

  completed!: number;
  notCompleted!: number;
  canceled!: number;

  loggedUser!: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private reportService: ReportService,
    private activityService: ActivityService,
    private sessionUser: SessionStorageService
  ) { }

  ngOnInit(): void {

    this.report = new Report();
    this.reportId = this.route.snapshot.params['id'];

    this.reportService.getReport(this.reportId).subscribe(data => {
      console.log(data)
      this.report = data;
    }, error => console.log(error));

    this.reloadData();
    this.loggedUser = this.sessionUser.getUser();
  }

  reloadData() {
    this.activityService.findActivities().subscribe(data => {
      this.activities = data;
    });
  }

  goToPDF(id: number) {
    this.router.navigate(['details', id, 'toPdf']);
  }

  gotoAddActivity(id: number) {
    this.router.navigate(['details', id, 'addactivity']);
  }

  updateActivity(id: number) {
    this.router.navigate(['updateActivity', id]);
  }

  deleteActivity(id: number) {
    this.activityService.deleteActivity(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
    this.reloadData();
  }

  countCompleted() {
    this.completed = 0;
    for (let i = 0; i < this.activities.length; i++) {
      if (this.activities[i].reportId == this.reportId && this.activities[i].status == "Įvykdyta") {
        this.completed++;
      }
    }
    return this.completed;
  }

  countNotCompleted() {
    this.notCompleted = 0;
    for (let a of this.activities) {
      if (a.reportId == this.reportId && a.status == "Vykdoma") {
        this.notCompleted = this.notCompleted + 1;
      }
    }
    return this.notCompleted;
  }

  countCanceled() {
    this.canceled = 0;
    for (let a of this.activities) {
      if (a.reportId == this.reportId && a.status == "Neįvykdyta") {
        this.canceled = this.canceled + 1;
      }
    }
    return this.canceled;
  }

  canUserEdit() {
    return (this.loggedUser.duties == this.report.committee);
  }
}

