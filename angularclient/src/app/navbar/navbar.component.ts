import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  loggedUser! : User;

  constructor(public loginService: AuthenticationService,  private sessionUser: SessionStorageService) {
  }

  ngOnInit(): void {
    this.loggedUser = this.sessionUser.getUser();
  }

  isAdmin(): boolean {
    if (this.loggedUser.duties == "Prezidentas" || this.loggedUser.duties == "Viceprezidentas" || this.loggedUser.duties == "Administratorius") {
      return true;
    }
    else
    return false;
  }

}
