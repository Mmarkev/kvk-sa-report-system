import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../activity';
import { ActivityService } from '../service/activity-service.service';
import { Report } from '../report';
import { ReportService } from '../service/report-service.service';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.css']
})
export class AddActivityComponent implements OnInit {

  id!: number;
  report!: Report;
  activity: Activity;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private activityService: ActivityService,
    private reportService: ReportService
  ) { this.activity = new Activity(); }

  ngOnInit(): void {
    this.report = new Report();
    this.id = this.route.snapshot.params['id'];

    this.reportService.getReport(this.id).subscribe(data => {
      this.report = data;
      this.activity.reportId = this.report.id;
    }, error => console.log(error));
  }

  gotoReport() {
    this.router.navigate(['details', this.report.id]);
  }

  gotoReports() {
    this.router.navigate(['details']);
  }

  onSubmit() {
    this.activityService.save(this.activity).subscribe(result => this.gotoReport());
  }
}
