import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { UserService } from '../service/user-service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = "";
  password = "";
  invalidLogin!: boolean;
  usersCheck!: boolean;


  constructor(private router: Router, private loginservice: AuthenticationService) { }
  ngOnInit() {
    this.checkUsers();
    this.checkInvalidLogin();
  }

  checkLogin() {
    if (this.loginservice.authenticate(this.email, this.password)) {
      this.router.navigate(['home'])
      this.invalidLogin = false

      if(this.loginservice.users != undefined) {
        this.usersCheck = false;
      }
      else {
        this.usersCheck = true;
        this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
          this.router.navigate(['']);
          console.log(this.usersCheck);
      }); 
      }
    }

    else {
      this.invalidLogin = true;
      this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
        this.router.navigate(['']);
        console.log(this.invalidLogin);
    }); 
    }
      
  }

  checkUsers() {
    return this.usersCheck;
  }

  checkInvalidLogin() {
    return this.invalidLogin;
  }

}
