import {Observable} from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../user';

@Injectable()
export class UserService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'https://reports-system-api.herokuapp.com/users';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  public updateUser(id: number, value: any): Observable<User> {
    return this.http.put<User>(`${this.usersUrl}/${id}`, value);
  }

  public updateUserPassword(id: number, duties: string, value: any): Observable<User> {
    return this.http.put<User>(`${this.usersUrl}/${id}/${duties}`, value);
  }

  public getUser(id: number): Observable<any> {
    return this.http.get(`${this.usersUrl}/${id}`);
  }

}