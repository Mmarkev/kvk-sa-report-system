import { Injectable } from '@angular/core';
import { User } from '../user';
import { UserService } from './user-service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  users!: User[];
  condition = false;

  constructor(private userService: UserService) { }

  authenticate(email: string, password: string) {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });

    for (let i = 0; i < this.users.length; i++) {
      if (email == this.users[i].email && password == this.users[i].password) {
        sessionStorage.setItem('email', email);
        this.condition = true;
      }
    }
    return this.condition;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('email')
    console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('email')
  }

  

}