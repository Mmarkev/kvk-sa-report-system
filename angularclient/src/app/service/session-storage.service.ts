import { Injectable } from '@angular/core';
import { User } from '../user';
import { UserService } from './user-service';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  users!: User[];
  loggedUser!: User;
  
  constructor(private userService: UserService) { }

  getUser() : User {
    var loggedEmail = sessionStorage.email;
    this.loggedUser = this.getSessionDetails(loggedEmail);
    return this.loggedUser;
  }

  getSessionDetails(email: string): User {
    this.getUsers();
    for (let i = 0; i < this.users.length; i++) {
      if (email == this.users[i].email) {
        this.loggedUser = this.users[i];
      }
    }
    return this.loggedUser;
  }

  getUsers() {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }

}
