import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { SessionStorageService } from '../service/session-storage.service';
import { User } from '../user';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  loggedUser! : User;
  
  constructor(public loginService: AuthenticationService, private sessionUser: SessionStorageService, private router: Router) {
  }

  ngOnInit(): void {
    this.loggedUser = this.sessionUser.getUser();
  }
  
}
