export class Report {
    id!: number;
    userId!: number;
    committee!: string;
    number!: string;
    template!: string;
    date!: string;
    creator!: string;
}
