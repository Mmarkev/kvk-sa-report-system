import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Report } from '../report';
import { ReportService } from '../service/report-service.service';

@Component({
  selector: 'app-update-report',
  templateUrl: './update-report.component.html',
  styleUrls: ['./update-report.component.css']
})
export class UpdateReportComponent implements OnInit {

  id!: number;
  report: Report;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private reportService: ReportService) {
    this.report = new Report();
  }

  ngOnInit() {
    this.report = new Report();

    this.id = this.route.snapshot.params['id'];

    this.reportService.getReport(this.id)
      .subscribe(data => {
        console.log(data)
        this.report = data;
      }, error => console.log(error));
  }

  updateReport() {
    this.reportService.updateReport(this.id, this.report)
      .subscribe(data => {
        console.log(data);
        this.report = new Report();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateReport();
  }

  gotoList() {
    this.router.navigate(['/reports/Bendros']);
  }

}