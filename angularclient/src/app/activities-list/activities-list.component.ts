import { Component, OnInit } from '@angular/core';
import { Activity } from '../activity';
import { Report } from '../report';
import { ActivityService } from '../service/activity-service.service';

@Component({
  selector: 'app-activities-list',
  templateUrl: './activities-list.component.html',
  styleUrls: ['./activities-list.component.css']
})
export class ActivitiesListComponent implements OnInit {

  reportId!: number;
  report!: Report;
  activities!: Activity[];

  constructor(
    private activityService: ActivityService
  ) { }


  ngOnInit(): void {
    this.reloadData();
  }

  reloadData() {
    this.activityService.findActivities().subscribe(data => {
      this.activities = data;
    });
  }

}