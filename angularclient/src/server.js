var express = require('express');
var path = require('path');
var app = express();
 
 
const port = process.env.PORT||8080;
//Set static Folder
app.use(express.static(path.join(__dirname, '../dist/angularclient')));
 
//Index routing 
app.get("/", function(req, res){
    res.sendFile(path.join(__dirname, '../dist/angularclient/index.html'));
    
});

  // Initialize the app.
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
  });
