package bd1;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.management.AttributeNotFoundException;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lt.kvk.i14.mm.bd.App;
import lt.kvk.i14.mm.bd.models.Activity;
import lt.kvk.i14.mm.bd.repositories.ActivityRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class testsForActivities {

	@Autowired
	private ActivityRepository activityRepo;
	
	private String testedActivityName = "Testavime pridėta veikla";

	@Test
	@Order(1)
	public void activityAddTest() throws AttributeNotFoundException {
		Activity activityToSave = new Activity(testedActivityName, "2021-02-12", "VF", "Įvykdyta",
				"Kokybiniai rezultatai", "Kiekybiniai rezultatai", 1);
		Activity activityAdd = activityRepo.save(activityToSave);
		assertNotNull(activityAdd);
	}

	@Test
	@Order(2) 
	public void activityGetByNameTest() {
		Activity activity = activityRepo.findByName(testedActivityName);
		assertEquals(testedActivityName, activity.getName());
	}

	@Test
	@Order(3) 
	public void activityUpdateTest() {
		Activity activity = activityRepo.findByName(testedActivityName);
		activity.setPlace("SvMF");

		activityRepo.save(activity);

		Activity activityUpdated = activityRepo.findByName(testedActivityName);

		assertEquals("SvMF", activityUpdated.getPlace());
	}

	@Test
	@Order(4) 
	public void activityDeleteTest() {
		long activityId = activityRepo.findByName(testedActivityName).getId();
		boolean activityExistBeforeDelete = activityRepo.findById(activityId).isPresent();
		
		activityRepo.deleteById(activityId);
		
		boolean activityNotExistBeforeDelete = activityRepo.findById(activityId).isPresent();
		
		assertTrue(activityExistBeforeDelete);
		assertFalse(activityNotExistBeforeDelete);

	}

}
