package bd1;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lt.kvk.i14.mm.bd.App;
import lt.kvk.i14.mm.bd.models.User;
import lt.kvk.i14.mm.bd.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
class TestsForUsers {

	@Autowired
	private UserRepository userRepo;
	
	long testedUserId = 1;

	@Test
	public void userGetByIdTest() {
		User user = userRepo.findById(testedUserId).get();
		assertEquals(testedUserId, user.getId());
	}

	@Test
	public void userUpdateTest() {
		String nameForTesting = "Testavimo vardas";
		User user = userRepo.findById(testedUserId).get();
		user.setName(nameForTesting);
		userRepo.save(user);

		User userUpdated = userRepo.findById(testedUserId).get();

		assertEquals(nameForTesting, userUpdated.getName());
	}

}
