package bd1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.management.AttributeNotFoundException;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lt.kvk.i14.mm.bd.App;
import lt.kvk.i14.mm.bd.repositories.ReportRepository;
import lt.kvk.i14.mm.bd.models.Report;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class testsForReports {

	@Autowired
	private ReportRepository reportRepo;
	
	private Report reportToTest = new Report(1, "10", "ARK", "Mėnesio ataskaita", "2021-02", "Vardenis Pavardenis");

	@Test
	@Order(1)
	public void reportAddTest() throws AttributeNotFoundException {
		Report reportAdd = reportRepo.save(reportToTest);
		assertNotNull(reportAdd);
	}

	@Test
	@Order(2)
	public void reportGetByIdTest() {
		Report report = reportRepo.findByCommitteeAndNumber(reportToTest.getCommittee(), reportToTest.getNumber());
		Optional<Report> reportById = reportRepo.findById(report.getId());
		assertNotNull(reportById);
	}

	@Test
	@Order(3)
	public void reportUpdateTest() {
		Report report = reportRepo.findByCommitteeAndNumber(reportToTest.getCommittee(), reportToTest.getNumber());
		report.setDate("2021-01");

		reportRepo.save(report);

		Report reportUpdated = reportRepo.findByCommitteeAndNumber(reportToTest.getCommittee(), reportToTest.getNumber());

		assertEquals("2021-01", reportUpdated.getDate());
	}

	@Test
	@Order(4)
	public void reportDeleteTest() {
		long testedReportId = reportRepo.findByCommitteeAndNumber(reportToTest.getCommittee(), reportToTest.getNumber()).getId();
		boolean reportExistBeforeDelete = reportRepo.findById(testedReportId).isPresent();
 
		reportRepo.deleteById(testedReportId);

		boolean reportNotExistBeforeDelete = reportRepo.findById(testedReportId).isPresent();

		assertTrue(reportExistBeforeDelete);
		assertFalse(reportNotExistBeforeDelete);

	}

}
