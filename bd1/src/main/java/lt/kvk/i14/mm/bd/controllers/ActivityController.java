package lt.kvk.i14.mm.bd.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.AttributeNotFoundException;
import javax.validation.Valid;

import org.springframework.boot.context.properties.source.InvalidConfigurationPropertyValueException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i14.mm.bd.models.Activity;
import lt.kvk.i14.mm.bd.repositories.ActivityRepository;

@RestController
@CrossOrigin(origins = "https://kvk-sa-reports-system.herokuapp.com")
public class ActivityController {

	private ActivityRepository activityRepository;

	public ActivityController(ActivityRepository activityRepository) {
		this.activityRepository = activityRepository;
	}

	@GetMapping("/activities")
	public List<Activity> getAllActivities() {
		return (List<Activity>) activityRepository.findAll();
	}

	@GetMapping("/activities/{id}")
	public ResponseEntity<Activity> getActivityById(@PathVariable(value = "id") Long activityId)
			throws AttributeNotFoundException {
		Activity activity = activityRepository.findById(activityId)
				.orElseThrow(() -> new AttributeNotFoundException("Activity not found for this id : " + activityId));
		return ResponseEntity.ok().body(activity);
	}

	@PostMapping("/activities")
	public void addActivity(@RequestBody Activity activity) {
		activityRepository.save(activity);
	}
	
	@PutMapping("/activities/{id}")
	public ResponseEntity<Activity> updateActivity(@PathVariable(value = "id") Long activityId,
			@Valid @RequestBody Activity updatedInfo) throws InvalidConfigurationPropertyValueException {
		Activity activity = activityRepository.findById(activityId).orElseThrow(
				() -> new InvalidConfigurationPropertyValueException("Activity not found for this id :: " + activityId,
						updatedInfo, null));

		activity.setName(updatedInfo.getName());
		activity.setDate(updatedInfo.getDate());
		activity.setPlace(updatedInfo.getPlace());
		activity.setStatus(updatedInfo.getStatus());
		activity.setQualRes(updatedInfo.getQualRes());
		activity.setQuanRes(updatedInfo.getQuanRes());
		final Activity updatedActivity = activityRepository.save(activity);
		return ResponseEntity.ok(updatedActivity);
	}
	
	@DeleteMapping("/activities/{id}")
	public Map<String, Boolean> deleteActivity(@PathVariable(value = "id") Long activityId)
			throws AttributeNotFoundException {
		Activity activity = activityRepository.findById(activityId)
				.orElseThrow(() -> new AttributeNotFoundException("Activity not found for this id : " + activityId));

		activityRepository.delete(activity);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
