package lt.kvk.i14.mm.bd.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "activities")
public class Activity {
	
	@Id
	@Column(name = "activityid")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "reportid")
	private long reportId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "place")
	private String place;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "qual_res")
	private String qualRes; 	//kokybiniai
	
	@Column(name = "quan_res")
	private String quanRes;		//kiekybiniai
	
	public Activity() {
	}
	
	public Activity(String name, String date, String place, String status, String qualRes, String quanRes, long reportId) {
		this.name = name;
		this.date = date;
		this.place = place;
		this.status = status;
		this.qualRes = qualRes;
		this.quanRes = quanRes;
		this.reportId = reportId;
	}
	
	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getQualRes() {
		return qualRes;
	}
	public void setQualRes(String qualRes) {
		this.qualRes = qualRes;
	}
	public String getQuanRes() {
		return quanRes;
	}
	public void setQuanRes(String quanRes) {
		this.quanRes = quanRes;
	}

	public long getReportId() {
		return reportId;
	}

	public void setReportId(long reportId) {
		this.reportId = reportId;
	}

}
