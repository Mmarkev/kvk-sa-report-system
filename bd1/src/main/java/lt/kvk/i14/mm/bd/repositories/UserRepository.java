package lt.kvk.i14.mm.bd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lt.kvk.i14.mm.bd.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
