package lt.kvk.i14.mm.bd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lt.kvk.i14.mm.bd.models.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long>{
	public Activity findByName(String name);
}
