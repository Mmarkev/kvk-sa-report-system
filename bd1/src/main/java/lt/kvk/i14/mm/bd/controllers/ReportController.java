package lt.kvk.i14.mm.bd.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.AttributeNotFoundException;
import javax.validation.Valid;

import org.springframework.boot.context.properties.source.InvalidConfigurationPropertyValueException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i14.mm.bd.models.Activity;
import lt.kvk.i14.mm.bd.models.Report;
import lt.kvk.i14.mm.bd.repositories.ActivityRepository;
import lt.kvk.i14.mm.bd.repositories.ReportRepository;

@RestController
@CrossOrigin(origins = "https://kvk-sa-reports-system.herokuapp.com")
public class ReportController {

	private ReportRepository reportRepository;
	private ActivityRepository activityRepository;

	public ReportController(ReportRepository reportRepository, ActivityRepository activityRepository) {
		this.reportRepository = reportRepository;
		this.activityRepository = activityRepository;
	}

	@GetMapping("/reports")
	public List<Report> getReports() {
		return (List<Report>) reportRepository.findAll();
	}

	@GetMapping("/reports/{id}")
	public ResponseEntity<Report> getReportById(@PathVariable(value = "id") Long reportId)
			throws AttributeNotFoundException {
		Report report = reportRepository.findById(reportId)
				.orElseThrow(() -> new AttributeNotFoundException("Report not found for this id : " + reportId));
		return ResponseEntity.ok().body(report);
	}

	@PostMapping("/reports")
	void addReport(@RequestBody Report report) {
		reportRepository.save(report);
	}

	@PutMapping("/reports/{id}")
	public ResponseEntity<Report> updateReport(@PathVariable(value = "id") Long reportId,
			@Valid @RequestBody Report updatedInfo) throws InvalidConfigurationPropertyValueException {
		Report report = reportRepository.findById(reportId).orElseThrow(
				() -> new InvalidConfigurationPropertyValueException("Report not found for this id :: " + reportId,
						updatedInfo, null));

		report.setNumber(updatedInfo.getNumber());
		report.setCommittee(updatedInfo.getCommittee());
		report.setTemplate(updatedInfo.getTemplate());
		report.setDate(updatedInfo.getDate());

		final Report updatedReport = reportRepository.save(report);
		return ResponseEntity.ok(updatedReport);
	}

	@DeleteMapping("/reports/{id}")
	public Map<String, Boolean> deleteReport(@PathVariable(value = "id") Long reportId)
			throws AttributeNotFoundException {
		Report report = reportRepository.findById(reportId)
				.orElseThrow(() -> new AttributeNotFoundException("Report not found for this id : " + reportId));
		
		
		List<Activity> activities = activityRepository.findAll();
		for (int i = 0; i < activities.size(); i++) {
			if (activities.get(i).getReportId() == reportId) {
				activityRepository.deleteById(activities.get(i).getId());
			}
		}
		reportRepository.delete(report);
		
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}