package lt.kvk.i14.mm.bd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lt.kvk.i14.mm.bd.models.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
	public Report findByCommitteeAndNumber(String committee, String number);
}
