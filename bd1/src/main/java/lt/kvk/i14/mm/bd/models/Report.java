package lt.kvk.i14.mm.bd.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reports")
public class Report {
	@Id
	@Column(name = "reportid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "userid")
	private long userId;
	
	@Column(name = "number")
	private String number;
	
	@Column(name = "committee")
	private String committee;
	
	@Column(name = "template")
	private String template;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "creator")
	private String creator;
	
	public Report() {
	};
	
	public Report(long userId, String number, String committee, String template, String date, String creator) {
		super();
		this.userId = userId;
		this.number = number;
		this.committee = committee;
		this.template = template;
		this.date = date;
		this.creator = creator;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getCommittee() {
		return committee;
	}

	public void setCommittee(String committee) {
		this.committee = committee;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
}
