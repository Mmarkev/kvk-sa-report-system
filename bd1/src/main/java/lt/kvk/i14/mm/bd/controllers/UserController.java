package lt.kvk.i14.mm.bd.controllers;

import java.util.List;

import javax.management.AttributeNotFoundException;
import javax.validation.Valid;

import org.springframework.boot.context.properties.source.InvalidConfigurationPropertyValueException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i14.mm.bd.models.User;
import lt.kvk.i14.mm.bd.repositories.UserRepository;

@RestController
@CrossOrigin(origins = "https://kvk-sa-reports-system.herokuapp.com")
public class UserController {
  
    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @GetMapping("/users")
    public List<User> getUsers() {
        return (List<User>) userRepository.findAll();
    }
    
  @GetMapping("/users/{id}")
	public ResponseEntity<User> getUser(@PathVariable(value = "id") Long userId)
			throws AttributeNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new AttributeNotFoundException("User not found for this id : " + userId));
		return ResponseEntity.ok().body(user);
	}
    
    @PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody User updatedInfo) throws InvalidConfigurationPropertyValueException {
		User user = userRepository.findById(userId).orElseThrow(
				() -> new InvalidConfigurationPropertyValueException("User not found for this id :: " + userId,
						updatedInfo, null));

		user.setName(updatedInfo.getName());
		user.setEmail(updatedInfo.getEmail());
		user.setDuties(updatedInfo.getDuties());

		final User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}
    
    @PutMapping("/users/{id}/{duties}")
   	public ResponseEntity<User> updateUserPassword(@PathVariable(value = "id") Long userId,
   			@Valid @RequestBody User updatedInfo) throws InvalidConfigurationPropertyValueException {
   		User user = userRepository.findById(userId).orElseThrow(
   				() -> new InvalidConfigurationPropertyValueException("User not found for this id :: " + userId,
   						updatedInfo, null));

   		user.setPassword(updatedInfo.getPassword());
   		
   		final User updatedUser = userRepository.save(user);
   		return ResponseEntity.ok(updatedUser);
   	}
}


